<?php

namespace Drupal\ebt_hero\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Transform Block settings in CSS.
 */
class GenerateHeroCSS implements ContainerInjectionInterface {

  /**
   * The EBT Core configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a new GenerateCSS object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('ebt_core.settings');
  }

  /**
   * Instantiates a new instance of this class.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Generate CSS from $settings.
   */
  public function generateFromSettings($settings, $block_class) {
    $hero_selector = '.' . $block_class . ' .ebt-block-hero__content';
    $col_1_selector = '.' . $block_class . ' .ebt-block-hero__content .col-1';
    $col_2_selector = '.' . $block_class . ' .ebt-block-hero__content .col-2';
    $hero_styles = '';

    $mobile_breakpoint = $settings['mobile_breakpoint'];
    if (empty($mobile_breakpoint)) {
      $mobile_breakpoint = $this->config->get('ebt_core_mobile_breakpoint');
    }

    if (empty($mobile_breakpoint)) {
      $mobile_breakpoint = '480';
    }
    $mobile_breakpoint = str_replace('px', '', $mobile_breakpoint);

    $hero_styles .= '@media screen and (max-width: ' . $mobile_breakpoint . 'px) { ';
    $hero_styles .= $col_1_selector . ' { width: 100%; } ';
    $hero_styles .= $col_2_selector . ' { width: 100%; } ';
    $hero_styles .= $hero_selector . ' { flex-direction: column; gap: 0; } ';
    $hero_styles .= ' } ';

    if (!empty($settings['image_position']) && $settings['image_position'] == 'right' &&
      ($settings['styles'] == 'two_columns_fluid' || $settings['styles'] == 'two_columns')) {
      $hero_styles .= $col_1_selector . ' { order: 2; } ';
      $hero_styles .= $col_2_selector . ' { order: 1; } ';
    }

    if (!empty($settings['image_order_mobile']) && $settings['image_order_mobile'] == 'image_first') {
      $hero_styles .= '@media screen and (max-width: ' . $mobile_breakpoint . 'px) { ';
      $hero_styles .= $col_1_selector . ' { order: 1; } ';
      $hero_styles .= $col_2_selector . ' { order: 2; } ';
      $hero_styles .= ' } ';
    }

    if (!empty($settings['image_order_mobile']) && $settings['image_order_mobile'] == 'image_last') {
      $hero_styles .= '@media screen and (max-width: ' . $mobile_breakpoint . 'px) { ';
      $hero_styles .= $col_1_selector . ' { order: 2; margin-top: 20px; } ';
      $hero_styles .= $col_2_selector . ' { order: 1; } ';
      $hero_styles .= ' } ';
    }

    if (!empty($settings['styles']) && $settings['styles'] == 'two_columns_fluid') {
      $hero_styles .= '@media screen and (max-width: ' . $mobile_breakpoint . 'px) { ';
      $hero_styles .= '.' . $block_class . '.two_columns_fluid .ebt-block-hero__content .col-1 .field--name-field-ebt-hero-column-image { ';
      $hero_styles .= ' width: auto; position: inherit; height: auto; ';
      $hero_styles .= ' } ';
      $hero_styles .= ' } ';
    }

    return '<style>' . $hero_styles . '</style>';
  }

}
