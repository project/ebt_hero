// --------------------------------------------------
// Load Plugins
// --------------------------------------------------

var gulp = require('gulp'),
    sass = require('gulp-dart-scss'),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    //sourcemaps = require("gulp-sourcemaps"),
    notify = require('gulp-notify'),
    sassUnicode = require('gulp-sass-unicode');

    // autoprefixer = require('gulp-autoprefixer'),
    // minifycss = require('gulp-clean-css'),
    // rename = require('gulp-rename'),
    // notify = require('gulp-notify'),
    // plumber = require('gulp-plumber'),
    // gutil = require('gulp-util'),
    // childprocess = require('child_process'),
    // sourcemaps = require('gulp-sourcemaps'),
    // merge = require('merge-stream'),
    // spritesmith = require('gulp.spritesmith')


var config = {
    // main scss files that import partials
    scssSrc: 'assets/scss/*.scss',
    // all scss files in the scss directory
    allScss: 'assets/scss/**/*.scss',
    // the destination directory for our css
    cssDest: 'assets/css/',
    // all js files the js directory
    allJs: 'assets/js/**/*.js',
    // all img files
    allImgs: 'assets/img/**/*'
};


// Define tasks after requiring dependencies
function style() {

  return gulp.src(config.allScss)
    //.pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sassUnicode())
//    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(postcss([autoprefixer()]))
    //.pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(config.cssDest));

  gulp.task('sass:watch', function () {
    gulp.watch('./sass/**/*.scss', ['sass']);
  });
}

// Expose the task by exporting it
// This allows you to run it from the commandline using
// $ gulp style
exports.style = style;

function watch(){
    // gulp.watch takes in the location of the files to watch for changes
    // and the name of the function we want to run on change
    gulp.watch('assets/scss/**/*.scss', style)
}

// Don't forget to expose the task!
exports.watch = watch
